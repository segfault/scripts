#!/bin/bash

# Just execute the original rake if this is not a build command
if [ "$1" != "build" ]; then
  exec /usr/bin/rake "$@"
fi

# Ensure we have sudo rights to prevent the prompt seconds after
# starting the build
sudo true

# Remove the chutney pycache which is created by root and causes a
# permission error
sudo rm -rf submodules/chutney/lib/chutney/__pycache__

# Always destroy the Tails builder VM before building. Not destroying
# it before building a different branch than during the last built will
# fail with:
#   error: pathspec '$BRANCH' did not match any file(s) known to git.
# Creating a new Tails builder is done in an instant, so this does not
# add any relevant overhead.
echo "Destroying build VM..."
/usr/bin/rake vm:destroy
echo "Building..."

# Don't delete the Tails build directory on failure. This allows
# easier debugging. The build directory is /tmp/tails-build*, the
# Tails filesystem is in the chroot/ directory inside the build dir.
# Since the build dir is under /tmp, it will be deleted anyway when
# the VM is shut down.
export TAILS_BUILD_FAILURE_RESCUE="${TAILS_BUILD_FAILURE_RESCUE:-1}"
echo "TAILS_BUILD_FAILURE_RESCUE: ${TAILS_BUILD_FAILURE_RESCUE}"

# Set options to speed up the build
export TAILS_BUILD_OPTIONS="${TAILS_BUILD_OPTIONS:-ram fastcomp cachewebsite}"
echo "TAILS_BUILD_OPTIONS: ${TAILS_BUILD_OPTIONS}"

# Execute rake and play a sound on exit
if time /usr/bin/rake "$@"; then
  notify-send "Tails build succeeded"
  play -q -n synth 0.1 sin 800 2>/dev/null
else
  notify-send "Tails build failed"
  play -q -n synth 0.1 sin 300 2>/dev/null
fi
