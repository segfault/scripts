#!/bin/bash

set -eou pipefail

DOMAIN="TailsToaster"
SNAPSHOTS_DIR="/tmp/TailsToaster/snapshots"
SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
GIT_REPO=$(realpath "${SCRIPT_DIR}/../..")
RUN_TEST_SUITE_SCRIPT="${GIT_REPO}/run_test_suite"

if [ "${EUID}" -ne 0 ]; then
  exec sudo --preserve-env "${0}" "$@"
fi

### Options
# If set, always delete snapshots without asking
DELETE_SNAPSHOTS=${DELETE_SNAPSHOTS-}
NO_DELETE_SNAPSHOTS=${NO_DELETE_SNAPSHOTS-}
# If set, apply the specified patch file before running the tests and
# then revert it again. This can be useful when you run git bisect and
# want to have temporary modifications during the bisect session.
WITH_PATCH=${WITH_PATCH-}
# If set, check out a worktree in tails-test-suite-worktree in the same
# directory as the Tails git repo
SEPARATE_WORKTREE=${SEPARATE_WORKTREE:-}

if [ -n "${SEPARATE_WORKTREE}" ]; then
  for arg in "$@"; do
    if [ "${arg}" = "--early-patch" ]; then
      echo >&2 "Using a separate worktree is incompatible with --early-patch"
      exit 1
    fi
  done

  WORKTREE_DIR=$(realpath "${GIT_REPO}/../tails-test-suite-worktree")

  # If there are uncommitted changes, create a new commit and check it
  # out in the worktree
  if [ -n "$(git status --porcelain)" ]; then
    git -C "${GIT_REPO}" commit -A -m "Temporary commit for test suite"
    COMMIT=$(git -C "${GIT_REPO}" rev-parse HEAD)
    # Remove the commit from the original worktree
    git -C "${GIT_REPO}" reset HEAD~1
  else
    COMMIT=$(git -C "${GIT_REPO}" rev-parse HEAD)
  fi

  if [ -d "${WORKTREE_DIR}" ]; then
    git -C "${WORKTREE_DIR}" checkout --quiet "${COMMIT}"
  else
    git -C "${GIT_REPO}" worktree add "${WORKTREE_DIR}" "${COMMIT}"
  fi

  cd "${WORKTREE_DIR}"
fi

if [ -n "${WITH_PATCH:-}" ]; then
  git apply < "${WITH_PATCH}"
fi

maybe_delete_snapshots () {
  local remote_snapshots=""
  if [ -d "${SNAPSHOTS_DIR}" ]; then
    remote_snapshots=$(find "${SNAPSHOTS_DIR}" -mindepth 1 -maxdepth 1 -type d)
  fi
  for snapshot in ${remote_snapshots}; do
    echo >&2 "Found snapshot ${snapshot}"
  done

  ### This can be deleted once remote snapshots are used on all branches ###
  local snapshot_files=""
  if [ -d /tmp/TailsToaster ]; then
    snapshot_files=$(find /tmp/TailsToaster -maxdepth 1 -name "*.memstate")
  fi

  local libvirt_snapshots=""
  if virsh domstate --domain "${DOMAIN}" >/dev/null 2>&1; then
    libvirt_snapshots=$(virsh snapshot-list --name "${DOMAIN}")
  fi
  ### End of section to be deleted ###

  if [ -z "${remote_snapshots}" ] && \
     [ -z "${snapshot_files}" ] && \
     [ -z "${libvirt_snapshots}" ]; then
    return
  fi

  if [ -z "${DELETE_SNAPSHOTS:-}" ]; then
    read -r -p "There are old snapshots. Do you want to delete them? (y/N) "
    echo
    if [ -z "${REPLY}" ] || ! [[ $REPLY =~ ^[Yy]$ ]]; then
      return
    fi
  fi

  for snapshot in ${libvirt_snapshots}; do
    echo "Deleting snapshot ${snapshot}"
    virsh snapshot-delete "${DOMAIN}" --snapshotname "${snapshot}"
  done

  for snapshot in ${snapshot_files}; do
    echo "Deleting snapshot ${snapshot}"
    rm -f "${snapshot}"
  done

  rm -rf "${SNAPSHOTS_DIR}"
}

if [ -n "${NO_DELETE_SNAPSHOTS:-}" ]; then
  echo >&2 "Skipping deletion of old snapshots"
else
  maybe_delete_snapshots
fi

# Set my preferred default configuration
export CAPTURE=${CAPTURE-yes}
export CAPTURE_ALL=${CAPTURE_ALL-yes}
export EXTRA_BOOT_OPTIONS=${EXTRA_BOOT_OPTIONS-"rootpw=root"}
export INTERACTIVE_DEBUGGING=${INTERACTIVE_DEBUGGING-yes}
export CHUTNEY_START_TIME=${CHUTNEY_START_TIME-90}
export KEEP_CHUTNEY=${KEEP_CHUTNEY-yes}
export KEEP_SNAPSHOTS=${KEEP_SNAPSHOTS-yes}
export NOTIFY_USER_COMMAND=${NOTIFY_USER_COMMAND-${SCRIPT_DIR}/tails-notify-test-suite}
export RESTRICT_CHUTNEY_MEMORY=${RESTRICT_CHUTNEY_MEMORY-yes}
export VCPUS=${VCPUS-4}
export VNC_VIEWER=${VNC_VIEWER-yes}
export VNC_VIEWER_VIEWONLY=${VNC_VIEWER_VIEWONLY-yes}
export VNC_SERVER=${VNC_SERVER-yes}

# Run the test suite
ret=0
"${RUN_TEST_SUITE_SCRIPT}" "$@" || ret=$?

# Kill any remaining tor processes
pkill -f "tor -f /tmp/TailsToaster" || true

if [ -n "${NOTIFY_USER_COMMAND:-}" ]; then
  if [ ${ret} -eq 0 ]; then
    TAILS_TEST_SUITE_SUCCESS=1 "${NOTIFY_USER_COMMAND}"
  else
    TAILS_TEST_SUITE_FAILED=1  "${NOTIFY_USER_COMMAND}"
  fi
fi

if [ -n "${WITH_PATCH:-}" ]; then
  git apply -R < "${WITH_PATCH}"
fi

exit "$ret"
